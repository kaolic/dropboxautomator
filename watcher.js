//
// Watches for incomming action files
//

var fs = require("fs"),
    hound = require("hound");

exports.name = "watcher";

exports.attach = function(opts) {
  watcher = hound.watch(opts.watchDirectory);
};

exports.init = function(done) {

  self = this;

  watcher.on("create", function(file, stats) {
    fs.readFile(file, function(err, data) {
      if (err) { throw err; }
      self.emit("action", data);
    });
  });

  return done();
};
