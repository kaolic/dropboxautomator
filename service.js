var config = require("./config"),
    broadway = require("broadway"),
    watcher = require("./watcher"),
    notifier = require("./notifier"),
    app = new broadway.App();

app.use(broadway.plugins.log, {
  logAll: true
});

//Load plugins
app.use(watcher, { "watchDirectory": config.watchDirectory });
//app.use(notifier, {});

app.init(function(err) {
  if (err) {
    throw err;
  }
});

app.on("action", function(action) {
  //action dispatcher
  console.log(action);
  //foreach
  app.emit(action.name, action);
});

app.on("sucess", function(action) {
  //notify if needed
  notifier.notify(action);
});

app.on("error", function(action) {
  //notify if needed
  notifier.notify(action);
});
